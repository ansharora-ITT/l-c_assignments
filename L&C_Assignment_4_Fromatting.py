# Linter used - PEP8
import json
import requests


def main():
    blogName = input(print("Enter the tumblr blog name"))
    startRange = int(input(print("enter start range")))
    endRange = int(input(print("enter end range")))
    fullURL = buildURL(blogName, endRange, startRange)
    URLresponse = openURL(fullURL)
    blogData = fetchAndConvertJSONData(URLresponse)
    displayBlogDetails(blogData)
    formBatches(blogName, startRange, endRange)


def buildURL(blogName, endOfRange, startOfRange):
    fullURL = "https://" + str(blogName) + ".tumblr.com/api/read/json?type=photo&num=" + str(endOfRange) + "&start=" + str(startOfRange) 
    return fullURL 


def openURL(fullURL): 
    URLresponse = requests.get(fullURL)
    return URLresponse


def fetchAndConvertJSONData(URLresponse):     
    if URLresponse.status_code == 200:
        RawData = URLresponse.content.decode('utf-8')
        JsonData = RawData[22:-2]    # Slicing to remove unrequired data
        DataInDictionary = json.loads(JsonData)    # converting JSON to Dictionary
        return DataInDictionary
    else:
        print("Cannot open URL")


def displayBlogDetails(blogData): 
    print("title:", blogData['tumblelog']['title'])
    print("name:", blogData['tumblelog']['name'])
    print("description:", blogData['tumblelog']['description'])
    print("no of posts:", blogData['posts-total'])


def formBatches(blogName, start, end):
    batchEnd = 0
    batch = 50
    while(batchEnd < end):
        batchLength = end - start
        if(batchLength > batch or batchLength == batch):
            batchEnd = (start + batch) - 1
            JSONdata = fetchAndConvertJSONData(openURL(buildURL(blogName, batch, start)))
            displayURLs(JSONdata, start, batch)
            start = batchEnd + 1

        else:
            if(start == end):
                batch = 1
            else:
                batch = (end - start) + 1
            batchEnd = end
            JSONdata = fetchAndConvertJSONData(openURL(buildURL(blogName, batch, start)))
            displayURLs(JSONdata, start, batch)


def displayURLs(blogData, start, batch):
    n = start
    for i in range(0, batch):
        if(len(blogData['posts'][i]['photos']) == 0):
            print(n, ". ", blogData['posts'][i]['photo-url-1280'])
        else:
            print(n, ". ", end="")
            for j in range(len(blogData['posts'][i]['photos'])):
                print(blogData['posts'][i]['photos'][j]['photo-url-1280'])
        print('')
        n += 1


main()
